//
//  WMGraphViewController.m
//  WM_GraphLib
//
//  Created by Christophe on 21/09/2016.
//  Copyright © 2016 Christophe. All rights reserved.
//

#import "WMGraphViewController.h"

@interface WMGraphViewController ()

@end

@implementation WMGraphViewController

#if 0
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
//-(void)viewDidAppear
#endif


-(void)setMaxForDraw:(CGFloat)max
{
    [_gvc_GraphImageView setMaxForDraw:max];
}
-(void)setLabelField:(__weak NSTextField *)label
{
    [_gvc_GraphImageView setLabelField:label];
}
//#if OLD
-(void)setBuffer:(const double *)buffer withSize:(NSUInteger)size /**< "size" is in reallity "length" (to be renamed) */
{
    [_gvc_GraphImageView setBuffer:buffer withSize:size];
}
//#else
-(void)useBuffer:(const WMBuffer *)iBuffer   {
    [_gvc_GraphImageView useBuffer:iBuffer];
}
//#endif

-(void)setStairs:(BOOL)stairs
{
    [_gvc_GraphImageView setStairs:stairs];
}
-(void)setTitle:(NSString *)title
{
    [_gvc_TitleTextField setStringValue:title];
}



@end
