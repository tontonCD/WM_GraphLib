//
//  WMGraphImageView.h (old: DW97TestImageView.h)
//  WM_GraphLib
//
//  Created by Christophe on 09/08/2016.
//  Copyright © 2016 Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WMBuffer.h"

// define one color as RGB: {CGFloat red, CGFloat green, CGFloat blue, CGFloat alpha}
#if 0
typedef CGFloat WMColors[4]; // OK but doesn't support copy
#else
typedef struct {
    CGFloat r, g, b, a;
} WMColors;
#endif

/**
 There's two ways for building the buffer, 
 - setBufferReference: (new), using your (WMBuffer *) instance,
 - setBuffer: (old), using your (double *) buffer, a (WMBuffer *) one is built internaly
 There's only one buffer, it can be a WMMultiBuffer
 */
@interface WMGraphImageView : NSImageView
{
#if OLD
    const double   *gtvBuffer;
    NSUInteger      gtvSize;
#else
    const WMBuffer *gtvWMBufferRef;
    WMBuffer       *gtvInternalWMBuffer;
#endif
    // v1.2:
    WMColors       *gtvLinecolor;
    //NSArray         *gtvLinecolors;   // means: CGFloat         gtvLinecolor[4];
    // v1.2:
    //int             gtvNumBuffers;
    
    CGFloat         gtvMax;         // for Draw
    CGFloat         gtvMaxFloored;  // for Draw
    __weak NSTextField *gtvLabel;   // the label for ordinates, i.e. the horizontal line defined by "y=gtvMaxFloored"
    
    BOOL            gtvStairs;
}
-(void)setMaxForDraw:(CGFloat)max;

-(void)setLabelField:(__weak NSTextField *)label; // TODO: private


//#if OLD
-(void)setBuffer:(const double *)buffer withSize:(NSUInteger)size; /**< "size" is in reallity "length" (to be renamed) */
//#else
-(void)useBuffer:(const WMBuffer *)buffer;/**< @since v1.2 */
//#endif
-(void)setStairs:(BOOL)stairs;

-(void)    setLineColor:(WMColors)color;    /**< @since v1.2 */
-(void)    setLineColor:(WMColors)iColor forBuffer:(NSUInteger)iBufferNum; /**< zero-based index */

//-(void)getLineColorTo:(WMColors)iColor; /**< @since v1.2 */
-(WMColors)lineColor; /**< @since v1.2 */

-(NSString *)getLabel; // TODO: private


#if DEBUG
# if OLD
-(const double *)getBuffer;
-(NSUInteger)    getBufferSize;
# else
-(const WMBuffer *)getBuffer;
# endif
#endif
@end
