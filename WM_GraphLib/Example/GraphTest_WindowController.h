//
//  GraphTest_WindowController.h
//  WM_GraphLib
//
//  Created by Christophe on 09/08/2016.
//  Copyright © 2016 Christophe. All rights reserved.
//


#import "WMGraphView.h"




# define REFERENCE_DATASET_8 {  1., 3., 8., 6., 7., 5., 4., 0.  }
# define REFERENCE_DATASET_4 {  1., 3., 8., 6.  }
# define REFERENCE_DATASET_1 {  1.  }
# define REFERENCE_DATASET_X REFERENCE_DATASET_8



@interface GraphTest_WindowController : NSWindowController
{
    // nothing
}

@property IBOutlet WMGraphView *graphView_L1;   // Left collumn, row #1
@property IBOutlet WMGraphView *graphView_L2;   // Left collumn, row #2
@property IBOutlet WMGraphView *graphView_L3;
@property IBOutlet WMGraphView *graphView_L4;

@property IBOutlet WMGraphView *graphView_R1;   // Right collumn, row #1
@property IBOutlet WMGraphView *graphView_R2;
@property IBOutlet WMGraphView *graphView_R3;
@property IBOutlet WMGraphView *graphView_R4;

-(IBAction)actionReset:(id)sender;
-(IBAction)actionDataset:(id)sender;


-(void)disposeBuffers;

@end
