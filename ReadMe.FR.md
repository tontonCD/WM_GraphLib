English Version [here](ReadMe.md).  
URL du projet : [https://gitlab.com/tontonCD/WM_GraphLib]() 

PROJET
======
**WM_GraphLib** est une librairie (API) permettant d'afficher vos données sous formes de graphiques dans des application MacOS.

FONCTIONNALITES
===============
- dessine à partir de buffers de type (double *), 
- possibilité d'afficher les courbes sous forme d'escalier,
- possibilité de donner un titre à chaque courbe,
- possibilité de changer la valeur maximum des données utilisée pour la magnification (zoom) ; cette valeur est recalculée à chaque changement de buffer.

Vous pouvez disposer autant de vues graphique (WMGraphView) que vous le souhaitez dans une même fenêtre.

La librairie intégre le code d'un exécutable servant d'exemple.
Extrait d'une copie d'écran de l'exemple :
![extrait d'une copie d'écran de l'application](./ReadMeImages/capt07_example.png)

- chaque vue est redimensionnable (via Interface Builder), et se redimensionne lorsque l'utilisateur de l'application change la taille de la fenêtre (Responsive Design)

![extrait 2 d'une copie d'écran de l'application](./ReadMeImages/capt08_resize.jpg)

- nouveau : dessine en couleur, 
- nouveau : peut dessiner plusieurs courbes par vue, 

![extrait 3 d'une copie d'écran de l'application](./ReadMeImages/capt09_ColorsAndMBuff.jpg)


INSTALLATION
============

Un projet exemple (Example/GraphTest_WindowController.m) est directement intégré au projet principal, qui produit un exécutable.

Pour vos propres projets voici la méthode conseillée :

1) télécharger le projet depuis GitLab et l'extraire à l'endroit qui convient, par exemple
/Users/Administrateur/wanmore/XCodeProjects/Libraries/

2) avec la console de commande, placez vous dans le dossier des fichiers source de votre projet devant utiliser la librairie, et y créer le dossier "headers" :
````console
$ cd /Users/Administrateur/wanmore/XCodeProject/ImageTracker/ImageTracker/
````
(par défaut le nom du projet appararaît deux fois dans la hiérarchie)
````
$ mkdir GraphLib
$ cd GraphLib
$ mkdir headers
```` 
3) Créer des lien symbolique des fichiers de la librairie vers ce dossier
````
$ ln -s /Users/Administrateur/wanmore/XCodeProject/Libraries/WM_GraphLib/WM_GraphLib/headers/WMGraph*.* headers/
````
Attention à bien utiliser le chemin complet vers la librairie, et non pas :
````
$ ln -s ../../../Libraries/WM_GraphLib/WM_GraphLib/headers/WMGraph*.* headers/ 
````
Faire de même avec le dossier "src":
````
$ mkdir src
$ ln -s /Users/Administrateur/wanmore/XCodeProject/Libraries/WM_GraphLib/WM_GraphLib/src/WMGraph*.* src/
````
3) Dans le Finder, sélectionnez les fichiers et faites les glisser vers la fenêtre Xcode de votre projet pour les y rajouter.
![WMGraphView.m dans Xcode](./ReadMeImages/capt05_path.png)

4) dans le header de votre WindowControler (.h), rajouter #import "WMGraphView.h", et rajouter à votre classe un membre de type WMGraphView,
5) dans IB, rajouter une Custom View (NSView) à votre interface (.xib), changer sa classe en WMGraphView, mapper les deux objets,
![WMGraphView dans Interface Builder](./ReadMeImages/capt06_link.png)

Votre projet doit compiler.

Si vous utilisez Git, votre prochain Commit génèrera surement des erreurs, car Xcode a considéré les fichiers rajoutés comme des fichiers réguliers et les a signalés comme tels à Git.

- première solution : les supprimer pour Git (pas pour le Finder) et les rajouter en ligne de commande :
````console
$ git rm --cached ./src/WMGraph*.*
$ git add ./src/WMGraph*.*
$ git ls-files -s ./src/
120000 e35dd715903d6fbbc0f25714ba0492be1f4d0fc6 0	src/WMGraphImageView.m
(...)
````
"120000" est le mode de fichier qui va bien (100644 pour les fichiers réguliers)

- deuxième solution (avancée, mais bien meilleure) : ajouter les entrées à votre fichier ".gitignore", et supprimer les comme précédemment (rm --cached).




VERSION : #2.0
==============
- support de la couleur pour les tracés,
- support de plusieurs séries de données pour une même vue.

A FAIRE
=======
- tracés de graphiques en "barre" ?
- supporter les données de type "int" (au lieu de "double") ?

NOTE
====
La librairie est utilisée telle quelle par un de mes projets, ImageTraker (non publié pour l'instant), qui manipule des entiers en grande quantité, d'où les améliorations proposées.


---
Remerciements au logiciel MkDocs (serveur de fichiers .ms/.markdown) qui m'a permis de formater ce document en local.
Merci de consulter la page du projet : [http://chdfree.free.fr/wanmore/index.php/wm_graphlib-moteur-de-rendu-graphique/](http://chdfree.free.fr/wanmore/index.php/wm_graphlib-moteur-de-rendu-graphique/) 
