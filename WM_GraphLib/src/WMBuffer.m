//
//  WMBuffer.m
//  WM_GraphLib
//
//  Created by Christophe on 21/09/2017.
//  Copyright © 2017 Christophe. All rights reserved.
//

#import "WMBuffer.h"

@implementation WMBuffer

+(instancetype)alloc
{
    WMBuffer *temp = [super alloc];
    if(temp)
    {
        temp->wmTitle = 0;
        
        temp->wmMasterBufferData = 0;
        temp->wmBufferLength = 0;
        
        temp->wmCurrentBuffer = 0;
    }
    return temp;
}


-(instancetype)initWithCapacity:(NSUInteger)length
{
    self = [super init];
    if(self)
    {
        if(wmMasterBufferData)
            free((void*)wmMasterBufferData);
        
        self->wmMasterBufferData = malloc(length * sizeof(double));
        
        self->wmCurrentBuffer = self->wmMasterBufferData;
        self->wmBufferLength = length;
    }
    return self;
}


-(void)_freeBuffers
{
    if(wmMasterBufferData)
    {
        // prevent any draws from wmBufferData
        if(wmCurrentBuffer == wmMasterBufferData)
        {
            wmCurrentBuffer = 0;
        }
        free((void*)wmMasterBufferData);
        wmMasterBufferData = 0;
    }
    
    //[super dealloc]; if ARC
}

-(void)dealloc  {
    [self _freeBuffers];
}

-(void)setData:(const double *)iBuffer withDataLength:(NSUInteger)iSize //please use: "useData:withDataLength:"
{
    [self useData:iBuffer withDataLength:iSize];
}

-(void)useData:(const double *)iBuffer withDataLength:(NSUInteger)iLength;
{
    [self _freeBuffers];
    
    wmCurrentBuffer = iBuffer; // (const double *)
    wmBufferLength = iLength;
    //gtvMax = 0;
    
    if(wmCurrentBuffer==0)
        wmBufferLength = 0;
}

-(void)setTitle:(NSString *)iTitle   {
    wmTitle = iTitle;
}


-(NSUInteger)length {
    return wmBufferLength;
}
-(NSString *)title  {
    return wmTitle;
}
-(const double *)data   {
    return wmCurrentBuffer;
}
-(const double *)dataAtNumBuffer:(NSUInteger)iNumBuffer  { // MULTI ???
    if(iNumBuffer<[self numBuffers])
        return &wmCurrentBuffer[iNumBuffer * wmBufferLength];
    return 0;
}

-(void)initData
{
#if 0
    NSUInteger dataIndex;
    double *myBufferData = (double *)wmBufferData; // we can, because the function is called explicitely
    
    for(dataIndex = 0; dataIndex < self->wmBufferLength; dataIndex++)
    {
        myBufferData[dataIndex] = 0;
    }
#else // CHECK
    NSUInteger totalLength = self->wmBufferLength * [self numBuffers];
    double zero = 0.0;
    
    double *myBufferData = (double *)wmCurrentBuffer; // we can, because the function is called explicitely

    memset(myBufferData /* void *__dst */, zero /* const void *__src */, sizeof(double)* totalLength /* size_t __n */);
#endif
}
-(void)copyDataFromBuffer:(WMBuffer *)iBuffer
{
    NSUInteger totalLength = self->wmBufferLength * [self numBuffers];
    NSUInteger fromBufferTotalLength = iBuffer->wmBufferLength * [iBuffer numBuffers];
    if( totalLength > fromBufferTotalLength )
        totalLength = fromBufferTotalLength; // the Less
    
    double *myBufferData = (double *)wmCurrentBuffer; // we can, because the function is called explicitely

    /* void* t = */ memcpy(myBufferData /* void *__dst */, iBuffer->wmCurrentBuffer /* const void *__src */, sizeof(double)* totalLength /* size_t __n */);
}

-(NSString *)description
{
    /*
     returns e.g.:
     @"<WMGraphView: 0x600000024cc0> { 0.25, 1.00, 3.00, 4.50, 6.00, 6.50, 5.50, 4.00, 2.25, 1.00, 0.00 }"
     */
    
    if(wmCurrentBuffer==0)
        return @"";
    if(wmBufferLength==0)
        return @"";

    NSMutableString *temp = [[NSMutableString alloc] init];
    [temp appendString:[NSString stringWithFormat:@"<WMGraphView: %p> { " , self]];

    NSUInteger numbuffers = [self numBuffers];
    if(numbuffers > 1)
    {
        [temp appendString:@" { "];
    }
    
    NSUInteger bufferIndex;
    for(bufferIndex = 0; bufferIndex<numbuffers; bufferIndex++)
    {
        // first data
        [temp appendString: [NSString stringWithFormat:@"%.2f", wmCurrentBuffer[0] ]];
        // continue
        for(UInt32 dsIndex = 1; dsIndex<wmBufferLength; dsIndex++)
            [temp appendString: [NSString stringWithFormat:@", %.2f", wmCurrentBuffer[dsIndex]] ];
    
        [temp appendString:@" }"];
        
        if(numbuffers > 1)
        {
            if(bufferIndex < numbuffers -1)
                [temp appendString:@", \n  {"];
            else
                [temp appendString:@" }"];
        }
    }
    
    return temp;
}

-(NSUInteger)numBuffers // not declared for one-buffer, only defined
{
    return 1;
}

-(CGFloat)findMin   {
    CGFloat minFound = 0.0;
    
    NSUInteger totalLength = wmBufferLength * [self numBuffers];
    
    UInt32 i;
    for(i = 0; i< totalLength; i++)
    {
        if(wmCurrentBuffer[i]<minFound)
            minFound = wmCurrentBuffer[i];
    }
    return minFound;
}
-(CGFloat)findMax   {
    CGFloat maxFound = 0.0;

    NSUInteger totalLength = wmBufferLength * [self numBuffers];

    UInt32 i;
    for(i = 0; i< totalLength; i++)
    {
        if(wmCurrentBuffer[i]>maxFound)
            maxFound = wmCurrentBuffer[i];
    }
    return maxFound;
}
-(CGFloat)findAbsMax    {
    CGFloat maxFound = 0.0;
    CGFloat minFound = 0.0;
    
    NSUInteger totalLength = wmBufferLength * [self numBuffers];

    UInt32 i;
    for(i = 0; i< totalLength; i++)
    {
        if(wmCurrentBuffer[i]>maxFound)
            maxFound = wmCurrentBuffer[i];
        if(wmCurrentBuffer[i]<minFound)
            minFound = wmCurrentBuffer[i];
    }
    CGFloat minmax = maxFound;
    if( (-minFound)>maxFound )
        minmax = -minFound;
    return minmax;
}

@end
