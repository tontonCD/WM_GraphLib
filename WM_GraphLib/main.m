//
//  main.m
//  WM_GraphLib
//
//  Created by Christophe on 11/07/2017.
//  Copyright © 2017 Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
