//
//  AppDelegate.m
//  WM_GraphLib
//
//  Created by Christophe on 11/07/2017.
//  Copyright © 2017 Christophe. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
    //     memLogger = [[WMMemLogger alloc] init];

    
    appTestWindow = [[GraphTest_WindowController alloc] initWithWindowNibName:@"GraphTest_WindowController"];
    
    // make the appTestWindow to be inited:
    NSWindow *theWindow = [appTestWindow window];
#pragma unused(theWindow)
    
    return;//TEMP
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return true;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    
    [appTestWindow disposeBuffers];

    //memLogger = 0;
}


@end
