//
//  WMBuffer.h
//  WM_GraphLib
//
//  Created by Christophe on 21/09/2017.
//  Copyright © 2017 Christophe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WMBuffer : NSObject
{
    NSString       *wmTitle;

    const double   *wmMasterBufferData; // was: wmBufferData, compatible with multibuffer
    NSUInteger      wmBufferLength;

    const double   *wmCurrentBuffer;    // for one-buffer case
}

/** Use this function if you want the buffer handles its own data */
-(instancetype)initWithCapacity:(NSUInteger)length;


/** if used, the buffer works with your data, you are responsible for deallocation */
-(void)setData:(const double *)buffer withDataLength:(NSUInteger)size __attribute__((deprecated(" renamed as useData:withDataLength:")));
/** if used, the buffer works with your data, you are responsible for deallocation */
-(void)useData:(const double *)buffer withDataLength:(NSUInteger)length;

-(void)setTitle:(NSString *)title;

-(NSUInteger)length;
-(NSString *)title;
-(const double *)data;
-(NSUInteger)numBuffers;                                /**< @return always 1 (unless it's a WMMultiBuffer) */
-(const double *)dataAtNumBuffer:(NSUInteger)numBuffer; /**< should use only it's a WMMultiBuffer @return 0 if bad numBuffer @see the "numBuffers" getter */

-(void)initData; /**< set all to zero */
-(void)copyDataFromBuffer:(WMBuffer *)buffer;

-(CGFloat)findMin;
-(CGFloat)findMax;
-(CGFloat)findAbsMax;

@end
