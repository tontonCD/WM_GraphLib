//
//  WMBuffer.h
//  WM_GraphLib
//
//  Created by Christophe on 21/09/2017.
//  Copyright © 2017 Christophe. All rights reserved.
//

#import "WMBuffer.h"

@interface WMMultiBuffer : WMBuffer
{
    NSUInteger  wmNumBuffers;
    
    // wmBufferLength means here the length for EACH buffer
}

/** Use this function if you want the buffer handles its own data */
-(instancetype)initWithCapacity:(NSUInteger)length andNumBuffers:(NSUInteger)numBuffers;

-(void)useData:(const double *)iBuffer inBuffers:(NSUInteger)iNumBuffers withBufferDataLength:(NSUInteger)iLength;

@end
