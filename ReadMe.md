Version Française [ici](ReadMe.FR.md).  
Project URL: [https://gitlab.com/tontonCD/WM_GraphLib]()

PROJECT
=======
**WM_GraphLib** is a code library (API) that makes your data display as mathematical curves in Macos programming.

FEATURES
========
- draw curves from (double *) buffers,
- possibility to switch between stairs or not,
- have a title for each graphic,
- possibility to change the data maximum value for magnification (zoom); this value is computed as auto, every time the buffer is changed.


You can display as many view (WMGraphView) that you wish in one window.
The library embeds some code for a launchable exemple.
Screenshot extract from this executable:
![screenshot extract from the application](./ReadMeImages/capt07_example.png)

- each vue is resizeable (via Interface Builder), and self resizes when the app user changes the window size (Responsive Design)

![extrait 2 d'une copie d'écran de l'application](./ReadMeImages/capt08_resize.jpg)


- new : draws with colors, 
- new : can draw many curves within one view, 

![extrait 3 d'une copie d'écran de l'application](./ReadMeImages/capt09_ColorsAndMBuff.jpg)


INSTALLATION
============

A project is given as an example (Example/GraphTest_WindowController.m), included in the main project. The main project produces an application.

For project from your own, here is how you may proceed:

1) download the project from Gitlab, extract it to a convenient path, e.g.
/Users/Administrateur/wanmore/XCodeProjects/Libraries/

2) using the Terminal application, go to the source files folder from your own projet, and make a new folder named "haders":
````console
$ cd /Users/Administrateur/wanmore/XCodeProject/ImageTracker/ImageTracker/
````
(by default the project name appears twice in the full path)
````
$ mkdir GraphLib
$ cd GraphLib
$ mkdir headers
````

3) Make symbolic links from the library files to this folder
````
$ ln -s /Users/Administrateur/wanmore/XCodeProject/Libraries/WM_GraphLib/WM_GraphLib/headers/WMGraph*.* headers/
````
Be aware to use the full path for the librarie, and not:
````
$ ln -s ../../../Libraries/WM_GraphLib/WM_GraphLib/headers/WMGraph*.* headers/ 
````
Act the same with the "src" folder:
````
$ mkdir src
$ ln -s /Users/Administrateur/wanmore/XCodeProject/Libraries/WM_GraphLib/WM_GraphLib/src/WMGraph*.* src/
````
3) From the Finder select these files and move them to your project Xcode window.
![WMGraphView.m in Xcode](./ReadMeImages/capt05_path.png)

4) in your WindowControler header (.h), add #import "WMGraphView.h", then declare a class member witch type is of WMGraphView,
5) within IB, add a Custom View (NSView) to your interface (.xib), change its class to WMGraphView, link them together(scr06link)
![WMGraphView in Interface Builder](./ReadMeImages/capt06_link.png)

Your projet may now compile.

If you use Git, you may encounter some complication when attempting to commit, it's because xcode added the files as regular files, and so it did with Git.

- first solution: remove them for Git (not the Finder) then add them in the Terminal:
````console
$ git rm --cached ./src/WMGraph*.*
$ git add ./src/WMGraph*.*
$ git ls-files -s ./src/
120000 e35dd715903d6fbbc0f25714ba0492be1f4d0fc6 0	src/WMGraphImageView.m
(...)
````
"120000" is the correct file mode (100644 for regular files)

- second solution (advanced, but much better): add the relevant entries to your ".gitignore" file, then remove the files for Git as just said (rm --cached)


VERSION: #2.0
===============
- color support for draws,
- can handle many data sets in the same view.

TODO
====
- have the graphic en "bars" ?
- support data as "int" (instead of "double") ?

NOTE
====
The library is used "as this" by another project from mines, ImageTraker (not still published), who handles hudge data as ints, so the reason for the actual "TODO" proposal. 


---
Thanks to the MkDocs software (file server and site generator for files of type .ms/.markdown), it made me possible to have localy the good representation.

Thanks to you for visit the project home page: [http://chdfree.free.fr/wanmore/index.php/wm_graphlib-moteur-de-rendu-graphique/](http://chdfree.free.fr/wanmore/index.php/wm_graphlib-moteur-de-rendu-graphique/) 
