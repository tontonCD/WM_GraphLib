//
//  WMGraphImageView.m (old: DW97TestImageView.m)
//  WM_GraphLib
//
//  Created by Christophe on 09/08/2016.
//  Copyright © 2016 Christophe. All rights reserved.
//

/* new : 
 
 offset from left/right borders
 data can be one-point-length
 "aluminium" color for axis, "turquoise" color for floor(max) axis, "mercury" color for plot
 accepts "Stairs"
 resizable
 
 todo? mark points
 todo? two (or more) curves
 */


#import "WMGraphImageView.h"
//#import "WMMultiBuffer.h"

//const CGFloat      black_color[4] =   { 0.0, 0.0, 0.0, 1.0 };
WMColors      black_color =   { 0.0, 0.0, 0.0, 1.0 };
//void colorcpy( WMColors dst, const WMColors src)    {
//    dst[0] = src[0];    dst[1] = src[1];    dst[2] = src[2];    dst[3] = src[3];    }
void setColorToCurrentContext(CGContextRef theContext, WMColors theColor)
{
    //CGContextSetRGBStrokeColor(theContext, theColor[0], theColor[1], theColor[2], theColor[3]);
    CGContextSetRGBStrokeColor(theContext, theColor.r, theColor.g, theColor.b, theColor.a);
}

//void copyColor(dest)

@implementation WMGraphImageView

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        gtvStairs = false;
        
        //gtvNumBuffers = 1;

        gtvLinecolor = malloc(1 * sizeof(WMColors) ); // assuming the buffer is one-buffer
        gtvLinecolor[0] = black_color;
        // colorcpy(gtvLinecolor[0], black_color); // temp, OBSO
        
    }
    return self;
}


-(void)_findMinMax
{
    CGFloat minmax;
    
    if(gtvWMBufferRef)
        minmax = [gtvWMBufferRef findAbsMax]; // should check if gtvWMBuffer exists, however minmax set to zero in this case
    else
    {
        gtvMax = gtvMaxFloored = 0.0;
        return;
    }
    
    if(minmax>0)
        gtvMax = 1.2 * minmax;
    else
        gtvMax = 0;
    CGFloat mult = 1.0;
    gtvMaxFloored = floor(gtvMax);
    // fix
    if(gtvMax>0)
    {
        while( (gtvMaxFloored = floor(mult * gtvMax))==0 )
            mult *= 10.0;
        gtvMaxFloored /= mult;
    }
#if DEBUG
    else
        printf("null data for gtvWMBufferRef: %p", gtvWMBufferRef); // not really an error
#endif
}

-(void)_freeBuffers
{
    //while(gtvNumBuffers)
    {
        gtvInternalWMBuffer = 0;
        gtvWMBufferRef = 0;
     
        if(gtvLinecolor)
            free(gtvLinecolor);
        gtvLinecolor = 0;

        //gtvNumBuffers--;
    }
}

#if 0 // OLD
// TEMP, usage: maintains the deprecated function setBuffer:withSize:
-(void)_buildBufferWithLength:(NSUInteger)iLen
{
    //gtvNumBuffers = 1; // build one
    
    gtvInternalWMBuffer = [[WMBuffer alloc] initWithCapacity:iLen];
    gtvWMBufferRef = gtvInternalWMBuffer;
    
}
#endif

//#if OLD
-(void)setBuffer:(const double *)iBuffer withSize:(NSUInteger)iSize
{
#if 0
    gtvBuffer = iBuffer; // (const double *)
    gtvSize = iSize;
    if(gtvBuffer==0)
        gtvSize = 0;
#else
 
    [self _freeBuffers];
    
    if(iSize)
    {
        //[self _buildBufferWithLength:iSize]; OBSO ?
        
        gtvInternalWMBuffer = [[WMBuffer alloc] init];
        [gtvInternalWMBuffer useData:iBuffer withDataLength:iSize];
        gtvWMBufferRef = gtvInternalWMBuffer;

        gtvLinecolor = (WMColors *)malloc( 1 * sizeof(WMColors) ); // always one-buffer while using "setBuffer:"
        gtvLinecolor[0] = black_color;
    }
    //else
    //{
    //    gtvInternalWMBuffer = 0;
    //    gtvLinecolor = 0;
    //}
#endif
    gtvMax = 0;
    
    
    [self _findMinMax];
}
//#else
-(void)useBuffer:(const WMBuffer *)iBuffer
{
    [self _freeBuffers];
    gtvWMBufferRef = iBuffer;
    
    if(iBuffer)
    {
        NSUInteger numBuffers = [iBuffer numBuffers];
        
        gtvLinecolor = (WMColors *)malloc( numBuffers * sizeof(WMColors) ); // TODO: update for multi
        for(int i = 0; i<numBuffers; i++)
            gtvLinecolor[i] = black_color;
        
        [self _findMinMax]; // should check ?
    }
}
//#endif

-(void)setMaxForDraw:(CGFloat)iMax
{
    gtvMax = iMax;
}
-(void)setLabelField:(__weak NSTextField *)iLabel
{
    gtvLabel = iLabel;
}
-(void)setStairs:(BOOL)iStairs
{
    gtvStairs = iStairs;
}
-(void)    setLineColor:(WMColors)iColor    {
    //colorcpy(gtvLinecolor, iColor);
    gtvLinecolor[0] = iColor;
}
-(void)    setLineColor:(WMColors)iColor forBuffer:(NSUInteger)iBufferNum    {
    //colorcpy(gtvLinecolor, iColor);
    if(iBufferNum < [gtvWMBufferRef numBuffers])
        gtvLinecolor[iBufferNum] = iColor;
}

#if 0
-(void)getLineColorTo:(WMColors)iColor  {
    colorcpy(iColor, gtvLinecolor);
}
#endif
-(WMColors)lineColor    {
    return gtvLinecolor[0];
}
-(NSString *)getLabel
{
    return gtvLabel.stringValue;
}

#if DEBUG
# if OLD
-(const double *)getBuffer      {
    return gtvWMBufferRef.data; };
-(NSUInteger)    getBufferSize  {
    return gtvWMBufferRef.length;   };
# else
-(const WMBuffer *)getBuffer    {
    return gtvWMBufferRef;
}
# endif
#endif


-(void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    if(gtvWMBufferRef==NULL)
        return;
    if(gtvWMBufferRef.length==0)
        return;
    
    
    
    NSUInteger bufferLength = gtvWMBufferRef.length;
    
    
    // CGFloat max = 7.0;// TEMP
    //    CGFloat maxFound = 0.0;
    
    // first margin for the Bessel Border
    CGRect bounds = [self bounds];
    bounds.origin.x +=     5;
    bounds.size.width -=  (5+4);
    bounds.origin.y +=     5;
    bounds.size.height -= (5+4);
    
    CGFloat xLeft =   bounds.origin.x;                      // drawable border at left
    CGFloat xRight =  bounds.origin.x + bounds.size.width;  // drawable border at right
    //CGFloat yMid = bounds.size.height / 2.0;
    CGFloat yMid = bounds.origin.y + bounds.size.height / 2.0; //  (Y + Y+W)/2
    CGFloat yTop =   bounds.origin.y;
    CGFloat yBottom = bounds.origin.y + bounds.size.height;
    
    const float marginV = 10.0;
    CGFloat y0 = yTop  + marginV;       // top limit for data
    CGFloat y1 = yBottom - marginV;     // bottom limit for data
    
    const float marginH = 10.0;
    CGFloat x0 = xLeft  + marginH;      // left limit for data
    CGFloat x1 = xRight - marginH;      // right limit for data
    CGFloat dx = 0;
    if(gtvStairs)
    {
        if(bufferLength>0)
            dx = (x1-x0) / (bufferLength);     //  |X..Y..Z...|
    } else{
        if(bufferLength>1)
            dx = (x1-x0) / (bufferLength - 1); //  |X..Y..Z|  , dx used (bufferLength - 1) times
    }
    
    CGFloat dy = 0;
    if(gtvMax)
        //dy = bounds.size.height / 2.0 / gtvMax;
        dy = (y1-y0) / 2.0 / gtvMax;
    
    
    /* ** lines ** */
#if 0
    //CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextRef currentContext = 0;
    NSGraphicsContext * currentGrContext = [NSGraphicsContext currentContext];
    //char *test = HOST_VERSION;
    //#if SDKROOT==HOST_VERSION
    if([currentGrContext respondsToSelector:@selector(CGContext)])
    {
        currentContext = [currentGrContext CGContext]; // >= X.10, NS_RETURNS_INNER_POINTER NS_AVAILABLE_MAC(10_10)
        
        //currentContext = [currentGrContext performSelector:@selector(CGContext)];
    }
    else
    {
        // e.g. >= macosx10.8
        NSGraphicsContext *gc = [currentGrContext graphicsPort];
        currentContext = [gc CGContext];
        //currentContext = (CGContextRef)gc;
    }
    //#endif
#else
    CGContextRef currentContext = [[NSGraphicsContext currentContext] graphicsPort];
#endif
    
    //??
    //CGContextSaveGState(currentContext);
    
    static CGFloat      color_aluminium[4] =   { .153/.255, .153/.255, .153/.255, 1.0 };
    static CGFloat      color_mercury[4] =     { .230/.255, .230/.255, .230/.255, 1.0 };
    static CGFloat      color_turquoise[4] =   { .0,        .255/.255, .255/.255, 1.0 };
    // static CGFloat   debug_color[4] =       { 0.2, 0.2, 1.0, 1.0 };
    // tangerine FF9900 ;
    // + (UIColor *)orangeColor;     // 1.0, 0.5, 0.0 RGB #cc4c00
    
    //CGContextSetRGBStrokeColor(currentContext, debug_color[0], debug_color[1], debug_color[2], debug_color[3]);
    /*
     static CGFloat      magentaColorComp[4];
     [[NSColor magentaColor] getComponents:magentaColorComp];
     CGColorRef orangeColorRef =  [[NSColor orangeColor] CGColor];
     CGContextSetStrokeColorWithColor(currentContext, orangeColorRef);
     */
    
    /* ** line at y=0 (X axis): ** */
    //CGContextSetStrokeColor(currentContext, color_aluminium); WRONG
    CGContextSetRGBStrokeColor(currentContext, color_aluminium[0], color_aluminium[1], color_aluminium[2], color_aluminium[3]);
    NSBezierPath *path = [[NSBezierPath alloc] init]; // could use:  CGContextMoveToPoint(context, 100, 100);
    [path moveToPoint:NSMakePoint(xLeft, yMid)];
    [path lineToPoint:NSMakePoint(xRight, yMid)];
    /* ** line at x=0 (Y axis): ** */
    [path moveToPoint:NSMakePoint(x0, yTop)];   // (not yInf)
    [path lineToPoint:NSMakePoint(x0, yBottom)];
    [path stroke];
    [path removeAllPoints];
    /* ** line at x=xmax : ** */
    CGContextSetRGBStrokeColor(currentContext, color_mercury[0], color_mercury[1], color_mercury[2], color_mercury[3]);
    [path moveToPoint:NSMakePoint(x1, yTop)];   // (not yInf)
    [path lineToPoint:NSMakePoint(x1, yBottom)];
    [path stroke];
    [path removeAllPoints];
    
    
    CGFloat ySup; // = yMid +  gtvMax * dy;
    CGFloat yInf; // = yMid - gtvMax * dy;
    /* ** line at gtvMax: ** */
    CGContextSetRGBStrokeColor(currentContext, color_turquoise[0], color_turquoise[1], color_turquoise[2], color_turquoise[3]);
    ySup = yMid +  gtvMaxFloored * dy;
    [path moveToPoint:NSMakePoint(xLeft, ySup)];
    [path lineToPoint:NSMakePoint(xRight, ySup)];
    /* ** line at -gtvMax: ** */
    yInf = yMid - gtvMaxFloored * dy;
    [path moveToPoint:NSMakePoint(xLeft, yInf)];
    [path lineToPoint:NSMakePoint(xRight, yInf)];
    if(gtvMax > 0)
        [path stroke];
    [path removeAllPoints];
    
    [gtvLabel setDoubleValue:gtvMaxFloored]; // (set position later)
    
    
    /* ** lines (data) ** */
    NSUInteger numBuffers = [gtvWMBufferRef numBuffers];
    for(int bufferIndex = 0; bufferIndex<numBuffers; bufferIndex++)
    {
        const double *bufferData = [gtvWMBufferRef dataAtNumBuffer:bufferIndex];
        
#if OLD
        CGContextSetRGBStrokeColor(currentContext, gtvLinecolor[0], gtvLinecolor[1], gtvLinecolor[2], gtvLinecolor[3]);
#else
        WMColors theBufferColor = gtvLinecolor[bufferIndex];
        CGContextSetRGBStrokeColor(currentContext, theBufferColor.r, theBufferColor.g, theBufferColor.b, theBufferColor.a);
        
        // TEST (OK);
        //setColorToCurrentContext(currentContext, theBufferColor);
#endif
        // first point:
        CGFloat x = x0;
        CGFloat y = yMid + bufferData[0] * dy;
        [path moveToPoint:NSMakePoint(x, y)]; // inline CGPoint pointFrom(i, Data)
        
        // loop:
        UInt32 i;
        for(i = 1; i< bufferLength; i++)
        {
            //        if(bufferData[i]>maxFound)
            //            maxFound = bufferData[i];
            
            x += dx;
            
            // added
            if(gtvStairs)
                [path lineToPoint:NSMakePoint(x, y)];
            
            y = yMid + bufferData[i] * dy;
            //x = i * dx;
            
            if(y<y0)
                y = y0;
            if(y>y1)
                y = y1;
            
            
            [path lineToPoint:NSMakePoint(x, y)];
        }
        if(gtvStairs)
        {
            x += dx;
            
            // added
            if(gtvStairs)
                [path lineToPoint:NSMakePoint(x, y)];
        }
        
        
        [path stroke];
        
        [path removeAllPoints];
    }
    // update the label position ; the line was drawn at ySup
    CGRect labelFrame = [gtvLabel frame];
    labelFrame.origin.y = ySup - labelFrame.size.height / 2;
    [gtvLabel setFrame:labelFrame];
    //[[gtvLabel superview] setNeedsDisplay:YES];
    [[gtvLabel superview] updateLayer];
    
    
}
@end
