//
//  WMGraphView.m
//  WM_GraphLib
//
//  Created by Christophe on 08/10/2016.
//  Copyright © 2016 Christophe. All rights reserved.
//

#import "WMGraphView.h"

@interface WMGraphView ()

@end

@implementation WMGraphView

#if USE_GW_CONTROLLER // false
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
//-(void)viewDidAppear
#else
-(void)viewDidHide
{
    
}
#endif
-(instancetype)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if(self)
    {
        self->gvGraphViewController = [[WMGraphViewController alloc] initWithNibName:@"WMGraphViewController.xib" bundle:nil];
    }
    return self;
}
-(instancetype)init
{
    self = [super init];
    if(self)
    {
        self->gvGraphViewController = [[WMGraphViewController alloc] initWithNibName:@"WMGraphViewController.xib" bundle:nil];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    //self = [self initWithNibName] -> cannot !
    
    NSView *graphViewController_View;
    //NSView *superView;
    if(self)
    {
        self->gvGraphViewController = [[WMGraphViewController alloc]
                                       initWithNibName:@"WMGraphViewController" bundle:nil];
        // instanciate the view:
        graphViewController_View = [self->gvGraphViewController view];
        
        [self addSubview:graphViewController_View];
        
        NSRect viewFrame = [self frame];
        NSRect graphViewFrame = [graphViewController_View frame];
        graphViewFrame.size = viewFrame.size;
        graphViewController_View.frame = graphViewFrame;
        
        [gvGraphViewController setLabelField:gvGraphViewController.gvc_MaxTextField];
        
        
        [self setTitle:@"(No Data)"];
        
        /* background */
        if([gvGraphViewController.gvc_GraphImageView layer]==0)
            [gvGraphViewController.gvc_GraphImageView setWantsLayer:YES];
        CALayer *layer = [gvGraphViewController.gvc_GraphImageView layer];
        [layer setBackgroundColor:CGColorGetConstantColor(kCGColorWhite)];
    }
    return self;
}

#if 0
// called (from NSView:) but not usefull
-(void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    //...
}
#endif

-(void)setMaxForDraw:(CGFloat)max
{
    //[_gv_ImageView setMaxForDraw:max];
    [gvGraphViewController.gvc_GraphImageView setMaxForDraw:max];
}
-(void)setLabelField:(__weak NSTextField *)label
{
    [gvGraphViewController setLabelField:label];
}

//#if OLD
-(void)setBuffer:(const double *)buffer withSize:(NSUInteger)size
{
    //[_gv_ImageView setBuffer:buffer withSize:size];
    [gvGraphViewController.gvc_GraphImageView setBuffer:buffer withSize:size];
    if(buffer==0)
        [gvGraphViewController.gvc_TitleTextField setStringValue:@"(No Data)"];
}
//#else
-(void)useBuffer:(const WMBuffer *)iBuffer
{
    [gvGraphViewController.gvc_GraphImageView useBuffer:iBuffer];
    if(iBuffer==0)
        [gvGraphViewController.gvc_TitleTextField setStringValue:@"(No Data)"];
}
//#endif

-(void)setStairs:(BOOL)stairs
{
    //[_gv_ImageView setStairs:stairs];
    [gvGraphViewController.gvc_GraphImageView setStairs:stairs];
}
-(void)setTitle:(NSString *)title
{
    //[_gv_TitleTextField setStringValue:title];
    [gvGraphViewController.gvc_TitleTextField setStringValue:title];
}
-(void)setLineColor:(WMColors)color
{
    [gvGraphViewController.gvc_GraphImageView setLineColor:color];
}
-(void)setLineColor:(WMColors)color forBuffer:(NSUInteger)iBufferNum // TODO: try to displace to WMMultibuffer
{
    [gvGraphViewController.gvc_GraphImageView setLineColor:color forBuffer:iBufferNum];
}
-(void)setNeedsDisplay:(BOOL)needsDisplay
{
    [super setNeedsDisplay:needsDisplay];
    //[gvGraphViewController setneeed
    
    [gvGraphViewController.gvc_GraphImageView setNeedsDisplay:YES];
}


// can be commented, or modified as you wish, actually prints the data to the Console
// TODO: extract as "extension"
-(void)mouseDown:(NSEvent *)theEvent
{
    [super mouseDown:theEvent];
    
#if OLD
    const double *buffer =  [gvGraphViewController.gvc_GraphImageView getBuffer];
    NSUInteger  bufferLen = [gvGraphViewController.gvc_GraphImageView getBufferSize];
    if(bufferLen==0)
        return;
#else
    const WMBuffer *buffer = [gvGraphViewController.gvc_GraphImageView getBuffer];
    // validity checked in the "description" function
#endif
    
#if OLD
    // print the data
    printf("%s: { ", [gvGraphViewController.gvc_TitleTextField.stringValue cStringUsingEncoding:NSMacOSRomanStringEncoding]);
    
    printf("%f", buffer[0]);
    for(UInt32 dsIndex = 1; dsIndex<bufferLen; dsIndex++)
        printf(", %.2f", buffer[dsIndex]);
    printf(" }\n");
#else
    printf("%s\n", [buffer.description cStringUsingEncoding:NSMacOSRomanStringEncoding]);
    // can use also: NSLog(@"%@", buffer.description);
    // or (in lldb): po buffer
#endif
    
    [gvGraphViewController.gvc_GraphImageView setNeedsDisplay:YES];
}
@end
