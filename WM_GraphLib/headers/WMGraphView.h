//
//  WMGraphView.h
//  WM_GraphLib
//
//  Created by Christophe on 08/10/2016.
//  Copyright © 2016 Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WMGraphImageView.h"
#import "WMBuffer.h"
#import "WMGraphViewController.h"

@interface WMGraphView : NSView           // VS: NSViewController
{
    WMGraphViewController *gvGraphViewController;
}

-(void)setMaxForDraw:(CGFloat)max;
-(void)setLabelField:(__weak NSTextField *)label;
-(void)setBuffer:(const double *)buffer withSize:(NSUInteger)size;
; 
-(void)useBuffer:(const WMBuffer *)buffer; /**< accepts 0 for the buffer, that means init @note the bufer is never released, you are responsible for this @note replaces "setBufferReference:" */

-(void)setStairs:(BOOL)stairs;
-(void)setTitle:(NSString *)title;      /**< changes the GraphView title */
-(void)setLineColor:(WMColors)color;    /**< changes the GraphView line color */
-(void)setLineColor:(WMColors)color forBuffer:(NSUInteger)iBufferNum; /**< zero-based index @note works only while if contains iBufferNum buffers, and colors are removed if useBuffer:nil is called */
// TODO: change this (keep colors)

@end
