//
//  AppDelegate.h
//  WM_GraphLib
//
//  Created by Christophe on 11/07/2017.
//  Copyright © 2017 Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>


#import "GraphTest_WindowController.h"



@interface AppDelegate : NSObject <NSApplicationDelegate>
{
///    MainWindowController *appFileMainWindow; // use array ? not now...
    GraphTest_WindowController *appTestWindow;
}

@end

