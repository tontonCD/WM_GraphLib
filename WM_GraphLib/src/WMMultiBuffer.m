//
//  WMBuffer.m
//  WM_GraphLib
//
//  Created by Christophe on 21/09/2017.
//  Copyright © 2017 Christophe. All rights reserved.
//

#import "WMMultiBuffer.h"

@implementation WMMultiBuffer


-(instancetype)initWithCapacity:(NSUInteger)length andNumBuffers:(NSUInteger)iNumBuffers
{
    self = [super init];
    if(self)
    {
        if(wmMasterBufferData)
            free((void*)wmMasterBufferData);
        
        self->wmNumBuffers = iNumBuffers;
        self->wmMasterBufferData = malloc(iNumBuffers * length * sizeof(double));
        
        self->wmCurrentBuffer = self->wmMasterBufferData;
        self->wmBufferLength = length;
    }
    return self;
}


-(void)_freeBuffers // TODO: declared tice, should use in one definition only, so declare it in WMBufferPrivate.h
{
    if(wmMasterBufferData)
    {
        // prevent any draws from wmBufferData
        if(wmCurrentBuffer == wmMasterBufferData)
        {
            wmCurrentBuffer = 0;
        }
        free((void*)wmMasterBufferData);
        wmMasterBufferData = 0;
    }
    
    //[super dealloc]; if ARC
}

-(void)useData:(const double *)iBuffer inBuffers:(NSUInteger)iNumBuffers withBufferDataLength:(NSUInteger)iLength
{
    //[self _freeBuffers]; // useless
    
#if 0 // seems good
    wmCurrentBuffer = iBuffer; // (const double *)
    wmBufferLength = iLength;
    //gtvMax = 0;
    
    if(wmCurrentBuffer==0)
        wmBufferLength = 0;
#else
    [super useData:iBuffer withDataLength:iLength];
    wmNumBuffers = iNumBuffers;
#endif
    //[self findMax]; // probably USELESS, check
}

-(NSUInteger)numBuffers {
    return wmNumBuffers;
}

@end
