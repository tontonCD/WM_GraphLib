//
//  GraphTest_WindowController.m
//  WM_GraphLib
//
//  Created by Christophe on 09/08/2016.
//  Copyright © 2016 Christophe. All rights reserved.
//

#import "GraphTest_WindowController.h"

#import "WMMultiBuffer.h" // useless if we use only the (single) WMBuffer class


/* ** data test ** */
static const SInt32 test1_numData =    8;
static const SInt32 test2_numData =   11;
static const SInt32 test3_numData =   11;
static const SInt32 test4_numData =   11;
static const SInt32 test4_numBuffers = 3; // 3 buffers x 11 elements

struct TEST_STRUCTURE {
    //SInt32 numData;
    //SInt32 waveOrder;
    //SInt32 maxIter;
    double dataset1[test1_numData];
    double dataset2[test2_numData];
    double dataset3[test3_numData];
    double dataset4[test4_numBuffers][test4_numData];
    
    //double output[10];
};

// instanciate 4 sets of data:
static const struct TEST_STRUCTURE test_structure = {
    /* data1: */ {  1.00,  3.00,  8.00,  6.00,  7.00,  5.00,  4.00,  0.00   },
    /* data2: */ {  0.25,  1.00,  3.00,  4.50,  6.00,  6.50,  5.50,  4.00,  2.25,  1.00,  0.00 },
    /* data3: */ { -0.25, -1.00, -2.50, -2.50, -0.50,  0.50,  1.00,  2.00,  2.25,  1.00,  0.00 },
    /* data4: */ {
        {  0.25,  1.00,  3.00,  4.50,  6.00,  6.50,  5.50,  4.00,  2.25,  1.00,  0.00 },
        { -0.25, -1.00, -2.50, -2.50, -0.50,  0.50,  1.00,  2.00,  2.25,  1.00,  0.00 },
        {  0.00, -0.50, -1.00, -2.50,  1.00, -0.50,  1.00,  0.50,  2.00,  0.00,  0.00 }         }
};

// declare 4 x (WMBuffer*)
#define SHOW_OLD_STYLE true // shows setting from (double*) VS WMBuffer
#if ! SHOW_OLD_STYLE //
WMBuffer *buffer1;
#else
// buffer1 not used in this case because of an OLD style example
#endif
WMBuffer *buffer2;
WMBuffer *buffer3;
WMMultiBuffer *buffer4;

/* ** ********* ** */


@interface GraphTest_WindowController ()

@end



@implementation GraphTest_WindowController

#if 0 // (useless)
-(void)windowWillLoad   {
    [super windowWillLoad];
}
#endif

-(void)dealloc
{
    [self disposeBuffers];
    
    //[WMMemLogger dealloc];
    //memLogger = 0;
}

#if 0 // (useless)
- (void)windowDidLoad {
    [super windowDidLoad];
}
#endif


#pragma mark - NSGestureRecognizerDelegate

#if 0 // OBSO ! (but would be called if you need)
-(void)mouseDown:(NSEvent *)theEvent
{
    // note: wasn't called because _window not set !
    
    [super mouseDown:theEvent];
    
    NSPoint where = [theEvent locationInWindow];
    //NSRect frame = [_mwImageView2_wave frame];
    
    /* check witch WMGraphView part */
    //if( NSPointInRect(where, [graphView_G1 frame]))  {
    //    [graphView_G1 setNeedsDisplay:YES];
    //    return;
    //}
}
#endif

-(void)disposeBuffers
{
    // prevent view update while buffer is no more valid:
    
#if SHOW_OLD_STYLE
    // old style (deprecated)
    [self.graphView_L1 setBuffer:nil withSize:0];  // NOTE: the title is set to &"(No Data)"
#else
    // new style
    [self.graphView_L1 useBuffer:nil];              // NOTE: the title is set to &"(No Data)"
#endif
    
    // new style
    [self.graphView_L2 useBuffer:nil];
    [self.graphView_L3 useBuffer:nil];
    [self.graphView_L4 useBuffer:nil];

#if SHOW_OLD_STYLE
    [self.graphView_R1 setBuffer:nil withSize:0];
#else
    [self.graphView_R1 useBuffer:nil];
#endif
    [self.graphView_R2 useBuffer:nil];
    [self.graphView_R3 useBuffer:nil];
    [self.graphView_R4 useBuffer:nil];
    
    
    // dispose the WMBuffer *
#if ! SHOW_OLD_STYLE
    buffer1 = 0;
#endif
    buffer2 = 0;
    buffer3 = 0;
    buffer4 = 0;

    
    // ???: reset all, for example the setMaxForDraw

}


-(IBAction)actionReset:(id)sender
{
    /* displays:
     *  phi0    PSI0
     *  phi1    PSI1
     */
    [self disposeBuffers];

    
    [self.graphView_L1 setNeedsDisplay:YES];
    [self.graphView_L2 setNeedsDisplay:YES];
    [self.graphView_L3 setNeedsDisplay:YES];
    [self.graphView_L4 setNeedsDisplay:YES];

    [self.graphView_R1 setNeedsDisplay:YES];
    [self.graphView_R2 setNeedsDisplay:YES];
    [self.graphView_R3 setNeedsDisplay:YES];
    [self.graphView_R4 setNeedsDisplay:YES];
}


-(IBAction)actionDataset:(id)sender
{
    [self disposeBuffers];
    double *data;
    UInt32 dataLength;

    // build 4 x (WMBuffer*), but only when using new style

#if 0
    // DON'T do this, use a persistent buffer (@see buffer1)
    WMBuffer *buffer1 = [[WMBuffer alloc] init]; // WRONG
#endif


#if ! SHOW_OLD_STYLE
    buffer1 = [[WMBuffer alloc] init];
#endif
    buffer2 = [[WMBuffer alloc] init];
    buffer3 = [[WMBuffer alloc] init];
    buffer4 = [[WMMultiBuffer alloc] init];

    
    data = (double *)test_structure.dataset1;
    dataLength = sizeof(test_structure.dataset1) / sizeof(double);

#if SHOW_OLD_STYLE
    [self.graphView_L1 setBuffer:data withSize:dataLength];
    [self.graphView_L1 setTitle:@"dataset 1"];
    [self.graphView_R1 setBuffer:data withSize:dataLength];
    [self.graphView_R1 setTitle:@"dataset 1 (Staired)"];
#else
    [buffer1 setData:data withDataLength:dataLength];
    [self.graphView_L1 setBuffer:buffer1];
    [self.graphView_L1 setTitle:@"dataset 1"];
    [self.graphView_R1 setBuffer:buffer1];
    [self.graphView_R1 setTitle:@"dataset 1 (Staired)"];
#endif
    
    data = (double *)test_structure.dataset2;
    dataLength = sizeof(test_structure.dataset2) / sizeof(double);
    [buffer2 useData:data withDataLength:dataLength];
    [self.graphView_L2 useBuffer:buffer2];
    [self.graphView_L2 setTitle:@"dataset 2"];
    [self.graphView_R2 useBuffer:buffer2];
    [self.graphView_R2 setTitle:@"dataset 2 (Staired)"];
    
    data = (double *)test_structure.dataset3;
    dataLength = sizeof(test_structure.dataset3) / sizeof(double);
    [buffer3 useData:data withDataLength:dataLength];
    [self.graphView_L3 useBuffer:buffer3];
    [self.graphView_L3 setTitle:@"dataset 3 (with color)"];
    [self.graphView_R3 useBuffer:buffer3];
    [self.graphView_R3 setTitle:@"dataset 3 (Staired)"];
   
    /* multibuffer (new) */
    data = (double *)test_structure.dataset4;
    [buffer4 useData:data inBuffers:test4_numBuffers withBufferDataLength:test4_numData];
    [self.graphView_L4 useBuffer:buffer4];
    [self.graphView_L4 setTitle:@"dataset 4 (multibuffer, with colors)"];
    [self.graphView_R4 useBuffer:buffer4];
    [self.graphView_R4 setTitle:@"dataset 4 (Staired)"];

    
#if 1 // OPTION
    [self.graphView_L1 setStairs:NO];
    [self.graphView_L2 setStairs:NO];
    [self.graphView_L3 setStairs:NO];
    [self.graphView_L4 setStairs:NO];
    
    [self.graphView_R1 setStairs:YES];
    [self.graphView_R2 setStairs:YES];
    [self.graphView_R3 setStairs:YES];
    [self.graphView_R4 setStairs:YES];

    /* NEW */
    [self.graphView_L3 setLineColor:(WMColors){0, 1.0, 0.0, 1.0}];      // green
    [self.graphView_R3 setLineColor:(WMColors){0, 0.0, 1.0, 1.0}];      // blue
    
    [self.graphView_L4 setLineColor:(WMColors){1.0, 0,   0,   1.0} forBuffer:0];    // red
    [self.graphView_L4 setLineColor:(WMColors){0,   1.0, 0,   1.0} forBuffer:1];    // green
    [self.graphView_L4 setLineColor:(WMColors){0,   0,   1.0, 1.0} forBuffer:2];    // blue
    [self.graphView_R4 setLineColor:(WMColors){1.0, 0,   0,   1.0} forBuffer:0];    // red
    [self.graphView_R4 setLineColor:(WMColors){0,   1.0, 0,   1.0} forBuffer:1];    // green
    [self.graphView_R4 setLineColor:(WMColors){0,   0,   1.0, 1.0} forBuffer:2];    // blue

#endif

    
    [self.graphView_L1 setNeedsDisplay:YES];
    [self.graphView_L2 setNeedsDisplay:YES];
    [self.graphView_L3 setNeedsDisplay:YES];
    [self.graphView_L4 setNeedsDisplay:YES];
    
    [self.graphView_R1 setNeedsDisplay:YES];
    [self.graphView_R2 setNeedsDisplay:YES];
    [self.graphView_R3 setNeedsDisplay:YES];
    [self.graphView_R4 setNeedsDisplay:YES ];
}



#if 0 // UNUSED in this project
-(DataBuffer *)_buildDataSet
-(bool)_checkPhi:(const double *)iPhi
-(bool)_checkPSI:(const double *)iPSI
#endif


     
@end
