//
//  WMGraphViewController.h
//  WM_GraphLib
//
//  Created by Christophe on 21/09/2016.
//  Copyright © 2016 Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WMGraphImageView.h"
#import "WMBuffer.h"

@interface WMGraphViewController : NSViewController // VS NSView

@property IBOutlet WMGraphImageView *gvc_GraphImageView; // was: gvc_DWImageView

@property IBOutlet NSTextField      *gvc_MaxTextField;
@property IBOutlet NSTextField      *gvc_TitleTextField;

-(void)setMaxForDraw:(CGFloat)max;
-(void)setLabelField:(__weak NSTextField *)label;
//#if OLD
-(void)setBuffer:(const double *)buffer withSize:(NSUInteger)size;
//#else
-(void)useBuffer:(const WMBuffer *)buffer;
//#endif
-(void)setStairs:(BOOL)stairs;
-(void)setTitle:(NSString *)title;

@end
